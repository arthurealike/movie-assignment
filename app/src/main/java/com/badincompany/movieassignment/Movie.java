package com.badincompany.movieassignment;

import android.widget.ImageView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Movie implements Comparable<Movie> {
    private int id;
    private String name;
    private String country;
   // private Array idGenre;
    private double avgVote;
    private String overview;
    private int img;

    public static int [] images = new int []{R.drawable.m1,R.drawable.m2,R.drawable.m3,R.drawable.m4,R.drawable.m5,R.drawable.m6,R.drawable.m7,R.drawable.m8,R.drawable.m9,
            R.drawable.m10,R.drawable.m11,R.drawable.m12,R.drawable.m13,R.drawable.m14,R.drawable.m15,R.drawable.m16,R.drawable.m17,R.drawable.m18,
            R.drawable.m19,R.drawable.m20
    };


    public Movie(int id, String name, String country, double avgVote, String overview) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.avgVote = avgVote;
        this.overview = overview;
    }

    public int compareTo(Movie m) {
         if(this.getAvgVote() > m.getAvgVote()) return 1;
           else if(this.getAvgVote() < m.getAvgVote()) return -1;
           return 0;

    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getAvgVote() {
        return avgVote;
    }

    public void setAvgVote(double avgVote) {
        this.avgVote = avgVote;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }
}
