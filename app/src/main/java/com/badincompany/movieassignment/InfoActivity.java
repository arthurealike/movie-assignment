package com.badincompany.movieassignment;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

public class InfoActivity extends AppCompatActivity {

    ImageView img;
    TextView tvmName,tvAvg,tvOw,tvCountry;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        //Hiding title bar using code
        getSupportActionBar().hide(); setContentView(R.layout.activity_info);

        // Hiding the status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        img = findViewById(R.id.img);
        tvmName = findViewById(R.id.tvmName);
        tvAvg = findViewById(R.id.tvAvg);
        tvCountry = findViewById(R.id.tvCountry);
        tvOw = findViewById(R.id.textView4);

        //               //              //

        //getting values from other class
        Intent i = getIntent();
        int idImage = Integer.parseInt(i.getStringExtra("img"));
        String avgVote = i.getStringExtra("avg");
        String name =  i.getStringExtra("name");
        String overview = i.getStringExtra("overview");
        String country = i.getStringExtra("country");


        //changing gui components accto values coming from other class
        img.setImageResource(idImage);
        tvmName.setText(name);
        tvCountry.setText(country);
        tvAvg.setText(avgVote);
        tvOw.setText(overview);
     //Movie m = new Movie();

    }
}
