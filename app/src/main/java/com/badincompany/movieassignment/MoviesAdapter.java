package com.badincompany.movieassignment;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ViewHolder> {
    Context _Context;
    Inflater inflater;
    private ArrayList<Movie> movieList = new ArrayList<>();

    public MoviesAdapter(Context _Context, ArrayList<Movie> movieList) {
        this._Context = _Context;
        this.movieList = movieList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView name;
            TextView avg;
            TextView overview;
            ImageView img;
            RatingBar ratingBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.name = itemView.findViewById(R.id.tvName);
            this.avg = itemView.findViewById(R.id.tvAverage);
            this.overview = itemView.findViewById(R.id.tvOverview);
            this.img = itemView.findViewById(R.id.imageView2);
            this.ratingBar = itemView.findViewById(R.id.ratingBar2);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(_Context,InfoActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            int position = getLayoutPosition();
            intent.putExtra("name",movieList.get(position).getName());
            intent.putExtra("avg",Double.toString(movieList.get(position).getAvgVote()));
            intent.putExtra("img", Integer.toString(movieList.get(position).getImg()));
            intent.putExtra("country",movieList.get(position).getCountry());
            intent.putExtra("overview",movieList.get(position).getOverview());
            _Context.startActivity(intent);
        }
    }

    /*
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(_Context,InfoActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _Context.startActivity(intent);
    }
    */

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
         LayoutInflater inflater = LayoutInflater.from(parent.getContext());
         View view = (View) inflater.inflate(R.layout.item_layout, parent, false);
         ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
            Movie m = movieList.get(position);
            holder.name.setText(m.getName());
            holder.avg.setText(Double.toString(m.getAvgVote()));
            holder.overview.setText(m.getOverview());
            holder.img.setImageResource(m.getImg());
            final float rating = (float) Double.parseDouble(holder.avg.getText().toString());
            holder.ratingBar.setRating(rating);

    }
    @Override
    public int getItemCount() {
        return movieList.size();
    }
}
