package com.badincompany.movieassignment;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity  {

    //JSON
    private JSONArray movies;
    private JSONObject movie;
    private RequestQueue _RequestQueue;

    private static final String MOVIES_URL = "https://gist.githubusercontent.com/suleymanccelik/2affad1fd704817558a0588957c96be9/raw/92c008eb01b3124c42e28dc05e12120d298876ef/movie_list.json";

    private ArrayList<Movie> movieList = new ArrayList<>();

    //Json fields
    private String TAG_ID = "id";
    private String TAG_NAME = "name";
    private String TAG_COUNTRY = "origin_country";
    private String TAG_VOTE = "vote_average";
    private String TAG_OVERVIEW = "overview";

    //RcyclerView
    private RecyclerView _RecyclerView;
    private MoviesAdapter _RecyclerAdapter;
    private RecyclerView.LayoutManager _LayoutManager;

    //public static final Intent intent = new Intent(this,InfoActivity.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Hiding title bar using code
        getSupportActionBar().hide(); setContentView(R.layout.activity_main);

        // Hiding the status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //////////////////////////////////////////////////////


        //Recycler Adapter
        _RecyclerView = (RecyclerView) findViewById(R.id.recycler1);
        _LayoutManager = new LinearLayoutManager(this);
        _RecyclerView.setLayoutManager(_LayoutManager);

        _RequestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest _JsonObjectRequest = new JsonObjectRequest (
                Request.Method.GET, MOVIES_URL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        movie = response;
                        new getMovies().execute();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Result",
                        "ERROR JSONObject request" + error.toString());
            }
        });
        _RequestQueue.add(_JsonObjectRequest);



       // Movie m = new Movie(1,"dark knight","US",9.2,"joker wins");
       // movieList.add(m);

        //images



        _RecyclerAdapter = new MoviesAdapter(MainActivity.this, movieList);
        _RecyclerView.setAdapter(_RecyclerAdapter);
        //Log.e("msg",m.getName());


    }

    //AsyncTask
    private class getMovies extends AsyncTask<Void,Void,Void> {

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                movies = movie.getJSONArray("results");
            }
            catch (JSONException ee) {
                ee.printStackTrace();
            }

            for(int i=0; i<movies.length();i++) {
               try {
                   JSONObject m = movies.getJSONObject(i);
                   int id = m.getInt(TAG_ID);
                   String name = m.getString(TAG_NAME);
                   String country = m.getString(TAG_COUNTRY);
                   double average = Double.parseDouble(m.getString(TAG_VOTE));
                   String overview = m.getString(TAG_OVERVIEW);
                   Movie _Movie = new Movie(id,name,country,average,overview);
                   movieList.add(_Movie);
                   Log.e("movies",movieList.get(i).getName());
                   }
               catch (JSONException ee) {
                   ee.printStackTrace();
               }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(movieList != null) {
                _RecyclerAdapter = new MoviesAdapter(MainActivity.this, movieList);
                _RecyclerView.setAdapter(_RecyclerAdapter);
                for(int i=0;i<movieList.size();i++) {
                        movieList.get(i).setImg(Movie.images[i]);
                }
            }else{
                Toast.makeText(MainActivity.this, "Not Found", Toast.LENGTH_LONG).show();
            }
        }
    }



}
